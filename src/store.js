import { configureStore } from '@reduxjs/toolkit'
import searchResultsSlice from './slicers/searchResultsSlice'

export const store = configureStore({
    reducer: {
      results: searchResultsSlice
  },
})