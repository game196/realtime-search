import React from "react";
import { BsSearch } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
import ResultsArea from "../components/ResultsArea";
import { saveResults } from "../slicers/searchResultsSlice";

function SearchPage() {
    const dispatch = useDispatch()
    const results = useSelector((state) => state.results);
    const { searchResults } = results

//   const [searchResults, setSearchResults] = useState(null);
  const queryHandler = (e) => {
    fetchResults(e.target.value);
  };

  const fetchResults = async (query) => {
    fetch(`https://mobile-staging.gametime.co/v1/search?q=${query}`)
      .then((response) => response.json())
      .then((data) => dispatch(saveResults(data)));
  };

  return (
    <div className="main">
      <div className="search-container">
        <h1 className="main-title">Events Search</h1>
        <div className="search-area">
          <BsSearch />
          <input
            type="text"
            placeholder="Search here"
            onChange={queryHandler}
          />
        </div>

        {searchResults && (
            <ResultsArea />
        )}
      </div>
    </div>
  );
}

export default SearchPage;
