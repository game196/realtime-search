import React from "react";
import { useSelector } from "react-redux";

function ResultsArea() {
    const results = useSelector((state) => state.results);
    const { searchResults } = results

  return (
    <div className="results-area">
      <h3>Events</h3>
      {searchResults.events.length === 0 ? (
        <p>No events found</p>
      ) : (
        <ul >
          {searchResults.events.slice(0, 3).map((data, index) => (
            <li key={index}>
              <img src={data.event.map_url} alt={""} />
              <div>
                <h3>{data.event.name}</h3>
                <p>{data.venue.name}</p>
              </div>
            </li>
          ))}
        </ul>
      )}
      <h3>Performers</h3>
      {searchResults.performers.length === 0 ? (
        <p>No performers found</p>
      ) : (
        <ul>
          {searchResults.performers.slice(0, 3).map((data, index) => (
            <li key={index}>
              <img src={data.hero_image_url} alt={""} />
              <div>
                <h3>{data.name}</h3>
                <p>{data.category_group}</p>
              </div>
            </li>
          ))}
        </ul>
      )}
      <h3>Venues</h3>
      {searchResults.venues.length === 0 ? (
        <p>No venues found</p>
      ) : (
        <ul>
          {searchResults.venues.slice(0, 3).map((data, index) => (
            <li key={index}>
              <img src={data.image_url} alt={""} />
              <div>
                <h3>{data.name}</h3>
                <p>{data.city}</p>
              </div>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}

export default ResultsArea;
