import { Provider } from "react-redux";
import SearchPage from "./pages/SearchPage";
import { store } from "./store";

function App() {
  return (
    <Provider store={store}>
      <SearchPage />
    </Provider>
  );
}

export default App;
